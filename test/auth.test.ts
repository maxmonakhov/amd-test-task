import * as Hapi from '@hapi/hapi';
import {Test, getServerInjectOptions} from './utils';
import {expect, describe, it, beforeAll, afterAll} from '@jest/globals';
import {getUUID} from '../src/server/utils';
import {ICredentials, ISignUpCredentials} from '../src/server/interfaces';

const jwtPattern = /^([A-Za-z0-9-_]+)\.([A-Za-z0-9-_]+)\.([A-Za-z0-9-_.]+)$/;

describe('Auth', () => {
	const HTTP_PATH = '/api/auth';

	let server: Hapi.Server;
	let res: any; // ServerInjectResponse

	let password: string = 'Password123!!';

	let access: string;
	let refresh: string;

	let email: string = `${getUUID()}@example.com`;

	const signUp: ISignUpCredentials = {
		email,
		password,
	};

	const specialistCred: ICredentials = {
		login: email,
		password,
	};

	beforeAll(async () => {
		server = await Test.start();
	});

	afterAll(async () => {
		await server.stop();
	});

	it('Registration', async () => {
		res = await server.inject(
			getServerInjectOptions(`${HTTP_PATH}/registration`, 'POST', null, signUp)
		);

		access = res.result.result.access;
		refresh = res.result.result.refresh;

		expect(access).toMatch(jwtPattern);
		expect(refresh).toMatch(jwtPattern);
		expect(res.statusCode).toEqual(200);
	});

	it('Login', async () => {
		res = await server.inject(
			getServerInjectOptions(`${HTTP_PATH}/login`, 'POST', null, specialistCred)
		);

		console.log('--- res.result', res.result);

		access = res.result.result.access;
		refresh = res.result.result.refresh;

		expect(access).toMatch(jwtPattern);
		expect(refresh).toMatch(jwtPattern);
		expect(res.statusCode).toEqual(200);
	});

	it('Refresh token', async () => {
		res = await server.inject(
			getServerInjectOptions(`${HTTP_PATH}/token/refresh`, 'POST', refresh)
		);

		expect(res.statusCode).toEqual(200);
		access = res.result.result.access;
		refresh = res.result.result.refresh;
	});

	it('Logout', async () => {
		res = await server.inject(getServerInjectOptions(`${HTTP_PATH}/logout`, 'POST', access));
		expect(res.statusCode).toEqual(200);
	});

	it('Logged out token cant be used anymore', async () => {
	res = await server.inject(getServerInjectOptions(`${HTTP_PATH}/logout`, 'POST', access));

	expect(res.result.ok).toEqual(false);
});
})
;
