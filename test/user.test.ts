import * as Hapi from '@hapi/hapi';
import { Test, getServerInjectOptions } from './utils';
import { expect, describe, it, beforeAll, afterAll } from '@jest/globals';
import { getUUID } from '../src/server/utils';
import { ISignUpCredentials } from '../src/server/interfaces';

describe('User', () => {
	const HTTP_PATH = '/api/users';

	let server: Hapi.Server;
	let res: any; // ServerInjectResponse

	let password: string = 'Password123!!';

	let access: string;

	let email: string = `${getUUID()}@example.com`;
	let userId: string;

	const signUp: ISignUpCredentials = {
		email,
		password,
	};

	beforeAll(async () => {
		server = await Test.start();

		res = await server.inject(
			getServerInjectOptions(`/api/auth/registration`, 'POST', null, signUp)
		);

		access = res.result.result.access;
		console.log('--- access', access);
	});

	afterAll(async () => {
		await server.stop();
	});

	it('Get all users', async () => {
		res = await server.inject(
			getServerInjectOptions(`${HTTP_PATH}/many?limit=10&offset=0`, 'GET')
		);

		userId = res.result.result.rows[0].id;

		expect(res.statusCode).toEqual(200);
	});

	it('Get all users with email filter', async () => {
		res = await server.inject(getServerInjectOptions(`${HTTP_PATH}/many?email=example`, 'GET'));

		expect(res.statusCode).toEqual(200);
	});

	it('Get user by id', async () => {
		res = await server.inject(getServerInjectOptions(`${HTTP_PATH}/one/${userId}`, 'GET'));

		expect(res.statusCode).toEqual(200);
	});

	it('Updates user', async () => {
		res = await server.inject(getServerInjectOptions(`${HTTP_PATH}/update`, 'POST', access, {firstName: 'New name', lastName: 'New last name'}));

		expect(res.statusCode).toEqual(200);
	});

	it('Get user statistics', async () => {
		res = await server.inject(getServerInjectOptions(`${HTTP_PATH}/statistics`, 'GET'));

		expect(res.statusCode).toEqual(200);
	});
});
