import { UserStatus, } from '../enums';
import { IPaginatedRequest, } from './common';

export interface IUserGetOne {
	id: string;
}

export interface IUserGetMany extends IPaginatedRequest {
	email?: string;
}

export interface IUserUpdate {
	firstName?: string;
	lastName?: string;
}


export interface PublicUser {
	id: string;
	email: string;
	phone: string;
	firstName?: string;
	lastName?: string;
	status: UserStatus;
}

export interface UsersStatistics {
	newUsersPerMonth: number;
}
