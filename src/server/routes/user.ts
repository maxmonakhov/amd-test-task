import { ServerRoute, } from '@hapi/hapi';
import * as api from '../api/user';
import * as user from '../schemas/user';

import { outputOkSchema, outputPaginationSchema, } from '../schemas/common';
import {AuthStrategy,} from '../enums';

export default <ServerRoute[]>[
	{
		method: 'GET',
		path: '/users/one/{id}',
		handler: api.getOne,
		options: {
			auth: false,
			id: 'user.getOne',
			description: 'Get user by id',
			tags: ['api', 'user'],
			validate: {
				params: user.getOneSchema,
			},
			response: {
				schema: outputOkSchema(user.userSchema),
			},
		},
	},
	{
		method: 'GET',
		path: '/users/many',
		handler: api.getMany,
		options: {
			auth: false,
			id: 'user.getMany',
			description: 'Get users. Optionally filter by email',
			tags: ['api', 'user'],
			validate: {
				query: user.getManySchema,
			},
			response: {
				schema: outputPaginationSchema(user.userSchema),
			},
		},
	},
	{
		method: 'POST',
		path: '/users/update',
		handler: api.update,
		options: {
			auth: AuthStrategy.JwtAccess,
			id: 'user.update',
			description: 'Update user',
			tags: ['api', 'user'],
			validate: {
				payload: user.updateSchema,
			},
			response: {
				schema: outputOkSchema(user.userSchema),
			},
		},
	},
	{
		method: 'GET',
		path: '/users/statistics',
		handler: api.getStatistics,
		options: {
			auth: false,
			id: 'user.statistics',
			description: 'Get users statistics',
			tags: ['api', 'user'],
			response: {
				schema: outputOkSchema(user.usersStatisticsSchema),
			},
		},
	}
];
