import { BelongsTo, Column, DataType, ForeignKey, Model, Table, } from 'sequelize-typescript';
import { getUUID, } from '../../utils';
import { User, } from './User';

@Table
export class Wallet extends Model {
	@Column({
		primaryKey: true,
		type: DataType.UUID,
		defaultValue: () => getUUID(),
	})
	override id!: string;

	@Column({
		type: DataType.BIGINT,
		allowNull: false,
		defaultValue: 0,
	})
		balance!: bigint;

	@ForeignKey(() => User)
	@Column({
		type: DataType.UUID,
		allowNull: false,
	})
		userId!: string;

	@BelongsTo(() => User)
		user!: User;
}
