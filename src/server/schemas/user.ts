import Joi from 'joi';
import { idSchema, paginatedRequestSchema, stringSchema, } from './common';

const userStatusSchema = Joi.object({
	color: Joi.string().valid('new', 'active', 'banned', 'deleted'),
});

export const userSchema = Joi.object({
	id: idSchema,
	email: Joi.string().email().required(),
	phone: Joi.string().required(),
	firstName: Joi.string().required(),
	lastName: Joi.string().required(),
	status: userStatusSchema,
});

export const getOneSchema = Joi.object({
	id: Joi.string().uuid().required(),
});

export const updateSchema = Joi.object({
	firstName: Joi.string(),
	lastName: Joi.string(),
})

export const getManySchema = paginatedRequestSchema().keys({
	email: stringSchema,
});

export const usersStatisticsSchema = Joi.object({
	newUsersPerMonth: Joi.number().min(0).integer(),
})
