import * as Hapi from '@hapi/hapi';
import {
	IOutputOk,
	IOutputPagination,
	IUserGetMany,
	IUserGetOne,
	IUserUpdate,
	PublicUser, UsersStatistics,
} from '../interfaces';
import { Boom, } from '@hapi/boom';
import { UserRepository, } from '../repositories';
import { Errors, ErrorsMessages, Exception, handlerError, outputOk, } from '../utils';
import { User, } from '../database/models';

export async function getOne(r: Hapi.Request): Promise<IOutputOk<PublicUser> | Boom> {
	const { id, } = r.params as IUserGetOne;

	try {
		const user = await UserRepository.findById(id);

		if (!user)
			throw new Exception(Errors.UserNotFound, ErrorsMessages[Errors.UserNotFound], {
				id,
			});

		const publicUser: PublicUser = toPublicUser(user);

		return outputOk(publicUser);
	} catch (err) {
		return handlerError('Failed to get user', err as Error);
	}
}

export async function getMany(r: Hapi.Request): Promise<IOutputPagination<PublicUser[]> | Boom> {
	const { email, ...pagination } = r.query as IUserGetMany;

	try {
		const usersResult = await UserRepository.findMany(email, pagination);

		const publicUsers: PublicUser[] = usersResult.rows.map((user) => toPublicUser(user));

		return {
			ok: true,
			result: { count: usersResult.count, rows: publicUsers, },
		};
	} catch (err) {
		return handlerError('Failed to get users', err as Error);
	}
}

export async function update(r: Hapi.Request): Promise<IOutputOk<PublicUser> | Boom> {
	const {firstName, lastName,} = r.payload as IUserUpdate;

	const user = r.auth.credentials.user! as { id: string };
	const id = user.id;

	try {
		const user = await UserRepository.findById(id);

		if (!user)
			throw new Exception(Errors.UserNotFound, ErrorsMessages[Errors.UserNotFound], {
				id,
			});

		user.firstName = firstName;
		user.lastName = lastName;

		await user.save();

		const publicUser: PublicUser = toPublicUser(user);

		return outputOk(publicUser);
	} catch (err) {
		return handlerError('Failed to update user', err as Error);
	}
}


export async function getStatistics(): Promise<IOutputOk<UsersStatistics> | Boom> {

	const lastMonth = new Date();
	lastMonth.setMonth(lastMonth.getMonth() - 1);

	try {
		const newUsersPerMonth = await UserRepository.getStatistics(lastMonth);

		const userStatistics: UsersStatistics = {
			newUsersPerMonth,
		}

		return outputOk(userStatistics);
	} catch (err) {
		return handlerError('Failed to get user', err as Error);
	}
}



function toPublicUser(user: User) {
	const { id, email, phone, firstName, lastName, status, } = user;
	const publicUser: PublicUser = {
		id,
		email,
		phone,
		firstName,
		lastName,
		status,
	};

	return publicUser;
}
