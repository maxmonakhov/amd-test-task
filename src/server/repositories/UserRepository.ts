import { Op, Transaction, WhereOptions, } from 'sequelize';
import { User, } from '../database/models';
import { UserStatus, } from '../enums';
import { IPaginatedRequest, } from '../interfaces';

interface IFindByEmailOptions {
	transaction?: Transaction;
}

interface IFindByLoginOptions {
	transaction?: Transaction;
	scope?: string;
}

interface ICreateOptions {
	transaction?: Transaction;
}

export class UserRepository {
	static async findByEmail(
		email: string,
		options: IFindByEmailOptions = {}
	): Promise<User | null> {
		const { transaction, } = options;

		return User.findOne({
			where: {
				email,
			},
			transaction,
		});
	}

	static async findById(id: string, options: IFindByEmailOptions = {}): Promise<User | null> {
		const { transaction, } = options;

		return User.findByPk(id, {
			transaction,
		});
	}

	static async findMany(
		email?: string,
		paginationOptions?: IPaginatedRequest,
		options: IFindByEmailOptions = {}
	) {
		const { transaction, } = options;
		const { offset = 0, limit = 10, } = paginationOptions || {};

		const whereOptions: WhereOptions<{ email: string }> = {};
		if (email) {
			whereOptions.email = { [Op.iLike]: `%${email}%`, };
		}

		const users = User.findAndCountAll({
			where: whereOptions,
			offset,
			limit,
			transaction,
		});

		return users;
	}

	static async findByLogin(
		login: string,
		options: IFindByLoginOptions = {}
	): Promise<User | null> {
		const { transaction, scope = 'defaultScope', } = options;

		return User.scope(scope).findOne({
			where: {
				[Op.or]: [
					{
						email: login,
					},
					{
						phone: login,
					}
				],
			},
			transaction,
		});
	}

	static async create(values: Partial<User>, options: ICreateOptions = {}): Promise<User | null> {
		const { transaction, } = options;

		return User.create(
			{
				...values,
				status: UserStatus.Active,
			},
			{
				transaction,
			}
		);
	}

	static async getStatistics(since: Date) {
		const newUsersPerMonth = await User.count({
			where: {
				createdAt: {
					[Op.gt]: since, // Use the between operator
				},
			},
		})

		return newUsersPerMonth;
	}
}
